import Data.Char (isAlpha, toLower)
import Control.Monad (forM, when)
import Data.Char (isAlpha)
import Control.Monad (forM)
import Data.Set (Set)
import qualified Data.Set as Set

main = do
  text <- readFile "ord.txt"
  let ordList = lines text
      empty = Set.empty
      wordSet = Set.map (map toLower) $ Set.filter (all isAlpha) $ foldl (flip Set.insert) empty ordList
      testword = "albin"
      wordLength = 6 --length testword
      wordsOfLength = Set.filter ((== wordLength) . length) wordSet
      wordsOfReverse = Set.filter (flip Set.member wordsOfLength . reverse) wordsOfLength
      words = wordsOfLength
  forM (Set.toList words) $ \e ->
    let file = show wordLength ++ "/" ++ e ++ ".txt"
        text = concatMap ((++ "\n") . unlines) $ allWords [e] words
    in when (length text > 0) $ do writeFile file text
                                   putStr text
--    putStr $ concatMap ((++ "\n") . unlines) $ allWords [e] words

startsWith :: String -> Set String -> Set String
-- find the words starting with a given substring
startsWith s w = Set.filter ((== s) . take (length s)) w

containsStart :: Set String -> [String] -> Int -> Bool
-- determines whether the dictionary contains words of correct start
containsStart _ [] _ = True
containsStart w (s:ss) n
 | n == length s = True
 | otherwise = containsStart w ss (n + 1) &&
               not (Set.null $ startsWith start w)
               where start = map (!! n) ss

nextWord :: Set String -> [String] -> [[String]]
-- given a list of words, returns all lists with an additional fitting word
nextWord w ss
 | length ss < 4 || containsStart w ss (length ss) = let start = map (!! length ss) ss
                                                         words = Set.toList $ startsWith start w
                                                     in map (ss ++) [[s] | s <- words]
 | otherwise = []

allWords :: [String] -> Set String -> [[String]]
allWords (s:ss) w = allWords' w (length s - length ss) [s:ss]
                    where allWords' _ _ [] = []
                          allWords' _ 1 ss = ss
                          allWords' w n ss = concatMap (allWords' w (n-1)) ns
                                             where ns = map (nextWord w) ss

{- varulv
   aralia
   raster
   ultima
   lieman
   varann

   smak
   mora
   arom
   kams

   kuk
   uhu
   kuk

   ö
-}
